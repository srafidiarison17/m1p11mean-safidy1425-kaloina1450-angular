import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.css']
})
export class LoginClientComponent {
  username: string='megmovmg';
  password: string='rakotonarivo';
  errorMessage: string='';

  constructor(private authService: AuthService, private router: Router,private storageService:StorageService) {}

  login() {
    this.errorMessage = ''; // Clear previous error message
    this.authService.login(this.username, this.password,'client').subscribe(response => {
      if(response!=null){
        this.storageService.saveUser(response);
        // sessionStorage.setItem('currentUser', JSON.stringify(response));  
        this.router.navigate(['/client/accueil']);
    }
    else{
    this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
    }
      console.log(response)
      // Handle successful login response, if needed
    }, error => {
      this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
      console.error('Login error:', error);
    });
  }
}