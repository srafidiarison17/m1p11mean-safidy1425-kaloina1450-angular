import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { StorageService } from 'src/app/services/storage.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';
@Component({
  selector: 'app-accueil-client',
  templateUrl: './accueil-client.component.html',
  styleUrls: ['./accueil-client.component.css'],
  
})
export class AccueilClientComponent implements OnInit{
  //  ab=JSON.parse( sessionStorage.getItem('currentUser'));
  user:any;
  constructor(private storageService:StorageService,private clientService:ClientService){
    const today = new Date();
    // this.date_rdv = today.toISOString().split('T')[0]; // Formater la date en format ISO
    this.date_rdv = this.formatDate(today);
  }
  showOptions:boolean=false;
  showHistory:boolean=false;

  services:any[]=[];
  selectedServices: number[] = []; // Tableau pour stocker les IDs des services sélectionnés
  showLoading: boolean = false; // Indicateur pour afficher l'écran de chargement
  showSuccessModal: boolean = false; // Indicateur pour afficher la fenêtre modale de réussite
  selectionServ:any[]=[];
  date_rdv:string ='';
  message:string='';
  history:any[]=[];
  payDone:boolean=false;

  ngOnInit(): void {
      this.user=this.storageService.getUser();
      this.clientService.getAllService().subscribe(
        (val)=>{this.services=val;
        
        console.log(this.services)},
        (error)=>{console.error(error)},
      );
        this.clientService.getMyRDV(this.user).subscribe(
          (succ)=>{this.history=succ;
          console.log(this.history)}
        );
  }

  toggleServiceSelection(serviceId: number,service:any=null): void {
    console.log(this.selectedServices)
    if (this.selectedServices.includes(serviceId)) {
        this.selectedServices = this.selectedServices.filter(id => id !== serviceId);
        this.selectionServ = this.selectionServ.filter(selectedService => selectedService._id !== serviceId);
    } else {
        this.selectedServices.push(serviceId);
        this.selectionServ.push(service);
    }
}
/**
 
 */
validerSelection(): void {
    console.log("Services sélectionnés :", this.selectedServices);
    this.showLoading = true;
    const values={
      // idService:this.selectedServices,
      service:this.selectionServ,
      client:this.user,
      etat:false,
      // message:'J\'aimerais prendre un rendez-vous pour ces services',
      // date:this.formatDate(new Date)
      date:this.date_rdv,
      message:this.message
    }
    this.clientService.takeRDV(values).subscribe();
    // Masquer l'écran de chargement après 2 secondes
    setTimeout(() => {
        this.showLoading = false;
        // Afficher la fenêtre modale de réussite
        this.showSuccessModal = true;
    }, 2000);
  }

  closeSuccessModal(): void {
    // Masquer la fenêtre modale de réussite
    this.showSuccessModal = false;
}

formatDate(date: Date): string {
  // Formater la date et l'heure en format ISO
  const year = date.getFullYear();
  const month = ('0' + (date.getMonth() + 1)).slice(-2);
  const day = ('0' + date.getDate()).slice(-2);
  const hour = ('0' + date.getHours()).slice(-2);
  const minute = ('0' + date.getMinutes()).slice(-2);

  return `${year}-${month}-${day}T${hour}:${minute}`;
}
dating(date:string): string{
  return date.replace('T',' ')
}

sumToPay(history: any) {
  if (history.service && Array.isArray(history.service)) {
    return history.service.reduce((acc: number, oneserv: any) => acc + Number(oneserv.prixService), 0);
  }
  return 0;
}

insertPayement(montant:number,id:string) {
  this.clientService.savePayement(this.user,montant).subscribe({
    next: data => {
      console.log(data);
      this.clientService.sendMail(this.user.email,montant).subscribe({
      });
      this.clientService.setEtatRdv(id).subscribe({
      });
    },
    error: err => {
    } 
  });
  }
}
