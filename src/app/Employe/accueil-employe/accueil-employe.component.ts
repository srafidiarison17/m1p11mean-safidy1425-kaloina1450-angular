import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-accueil-employe',
  templateUrl: './accueil-employe.component.html',
  styleUrls: ['./accueil-employe.component.css']
})
export class AccueilEmployeComponent implements OnInit{
  //  ab=JSON.parse( sessionStorage.getItem('currentUser'));
  user:any;
  constructor(private storageService:StorageService,private employeService:EmployeService){}
  rdvEmploye:any[]=[];

  showRdv:boolean=false;
  jobDone:boolean=false;
  tache:boolean=false;

  client:any;
  service:any;

  selectedRdvId: string='';
  selectedServiceId: any;
  selectedClient: any;
  selectedService: any;
  selectedRdv: any;
  tachesdone:any[]=[];


  ngOnInit(): void {
      this.user=this.storageService.getUser();
      this.employeService.getRdvEmploye().subscribe(
        (val)=>{this.rdvEmploye=val;},
        (error)=>{console.error(error)},
      );
      this.employeService.getTaches(this.user._id).subscribe(
        (val)=>{this.tachesdone=val;},
        (error)=>{console.error(error)},
      );
  }

  clickCheck(service:any,rdv:any)
  {
    this.selectedService=service;
    this.selectedRdv=rdv;
  }
  
  jobdone()
  {
    console.log(this.selectedService.nomService);
    console.log(this.selectedRdv.message);
    this.employeService.setEtatServ(this.selectedRdv._id,this.selectedService._id).subscribe({
      next: data => {
        console.log(data);
        this.employeService.saveJobDone(this.selectedRdv.client,this.selectedService,this.user).subscribe({
        });
        this.jobDone=true;
      },
      error: err => {
      } 
    });
  }

  commission(tache:any)
  {
    return Number(tache.service.prixService) * Number(tache.service.commissionService) / 100;
  }
}
