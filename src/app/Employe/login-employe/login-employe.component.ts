import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-login-employe',
  templateUrl: './login-employe.component.html',
  styleUrls: ['./login-employe.component.css']
})
export class LoginEmployeComponent {
  username: string = 'loi@gmail.com';
  password: string = 'loloo';
  errorMessage: string='';

  constructor(private authService: AuthService,private router: Router,private storageService:StorageService) {}

  login() {
    this.errorMessage = ''; // Clear previous error message
    this.authService.login(this.username, this.password,'employe').subscribe(response => {
      if(response!=null){
        this.storageService.saveUser(response);
        // sessionStorage.setItem('currentUser', JSON.stringify(response));  
        this.router.navigate(['/employe/accueil']);    }
    else{
    this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
    }
      console.log(response)
      // Handle successful login response, if needed
    }, error => {
      this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
      console.error('Login error:', error);
    });
  }
}
