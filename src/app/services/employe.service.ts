import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class EmployeService {

  // private apiUrl = 'http://localhost:3000'; // Replace this with your API URL
  private apiUrl='https://m1p11mean-safidy1425-kaloina1450.onrender.com';


  constructor(private http: HttpClient,private router:Router) { }
  getRdvEmploye(): Observable<any>{
    return this.http.get(this.apiUrl+"/getRdvs");
  }

  setEtatServ(id:string,idServ:string):Observable<any>
  {
    return this.http.put(
      this.apiUrl+'/setEtatServ',
      {
         id,idServ
      },
      httpOptions
    );
  }

  saveJobDone(client:any,service:any,employe:any): Observable<any>  {
    return this.http.post(
      this.apiUrl+'/insertJobDone',
      {
        client,
        service,
        employe
      },
      httpOptions
    );
  }

  getTaches(idEmp: string): Observable<any> {
    const url = this.apiUrl+'/getJobDone';
    const params = new HttpParams().set('idemploye',idEmp);
    return this.http.get(url, { params });
  }
}
