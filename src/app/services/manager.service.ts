import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
  // private apiUrl = 'http://localhost:3000'; // Replace this with your API URL
  private apiUrl='https://m1p11mean-safidy1425-kaloina1450.onrender.com';


  constructor(private http: HttpClient,private router:Router) { }

  saveDepense(titre:string,montant:number,daty:string,type:string,encaisseur:string): Observable<any>  {
    return this.http.post(
      this.apiUrl+'/insertDepense',
      {
        titre,
        montant,
        type,
        encaisseur,daty
      },
      httpOptions
    );
  }

  getStatRdv(year=2024): Observable<any>
  {
    return this.http.get(this.apiUrl+"/rdvByDate");
  }
  getStatPaiement(year=2024): Observable<any>
  {
    return this.http.get(this.apiUrl+"/paiementByDate");
  }
 getBenefice(year=2024): Observable<any>
 {
       return this.http.get(this.apiUrl+"/benefice");

 }
}
