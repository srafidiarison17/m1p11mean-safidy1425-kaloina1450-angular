import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})

export class ClientService {
  // private apiUrl = 'http://localhost:3000'; // Replace this with your API URL
  private apiUrl='https://m1p11mean-safidy1425-kaloina1450.onrender.com';


  constructor(private http: HttpClient,private router:Router) { }
  
  getAllService(): Observable<any>{
    return this.http.get(this.apiUrl+"/getServices");
  }

  takeRDV(rdv:any):Observable<any>{
    return this.http.post(this.apiUrl+"/takeRDV",rdv,httpOptions);

    return of('null')
  }
  getMyRDV(user:any) :Observable<any>{
    console.log(user);
    const params = new HttpParams().set('client',user._id);

    return this.http.get(this.apiUrl+"/getRdvClient",{params});
  }

  savePayement(client:any,montant:number): Observable<any>  {
    return this.http.post(
      this.apiUrl+'/insertPayement',
      {
        client,montant
      },
      httpOptions
    );
  }

  sendMail(recipient:string,montant:number):Observable<any>
  {
    const content='Vous venez de payer '+montant+' Ariary au salon SK. Merci beaucoup';
    return this.http.post(
      this.apiUrl+'/sendMail',
      {
        recipient,content
      },
      httpOptions
    );
  }

  setEtatRdv(id:string):Observable<any>
  {
    return this.http.put(
      this.apiUrl+'/setEtatRdv',
      {
         id
      },
      httpOptions
    );
  }
}
