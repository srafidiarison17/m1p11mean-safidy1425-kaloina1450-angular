import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  // private apiUrl = 'http://localhost:3000'; // Replace this with your API URL
  private apiUrl='https://m1p11mean-safidy1425-kaloina1450.onrender.com';

  constructor(private http: HttpClient,private router:Router) { }

  login(email: string, password: string,user:string): Observable<any> {
    if(user=='client')
    return this.http.post<any>(`${this.apiUrl}/authentClient`, { email, password },httpOptions);
    
    else if (user=='employe')
    return this.http.post<any>(`${this.apiUrl}/authentEmploye`, { email, password },httpOptions);
    
    else
    return this.http.post<any>(`${this.apiUrl}/authentManager`, { email, password },httpOptions);
  }

  logout(): void {
    localStorage.removeItem('currentUser')
    this.router.navigate(['/authentification']);  
  }
  register(name: string, email: string, password: string): Observable<any> {
    return this.http.post(
      this.apiUrl + '/insertClient',
      {
        name,
        email,
        password,
      },
      httpOptions
    );
  }

  saveService(nomService:string,prixService:number,dureeService:number,commissionService:number,descriService:string): Observable<any>  {
    return this.http.post(
      this.apiUrl+'/insertService',
      {
        nomService,
        prixService,
        dureeService,
        commissionService,
        descriService
      },
      httpOptions
    );
  }

}