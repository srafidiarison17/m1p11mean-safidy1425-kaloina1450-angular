import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginEmployeComponent } from './Employe/login-employe/login-employe.component';
import { LoginClientComponent } from './Client/login-client/login-client.component';
import { LoginManagerComponent } from './Manager/login-manager/login-manager.component';
import { AccueilEmployeComponent } from './Employe/accueil-employe/accueil-employe.component';
import { AccueilClientComponent } from './Client/accueil-client/accueil-client.component';
import { AccueilManagerComponent } from './Manager/accueil-manager/accueil-manager.component';
import { RegisterClientComponent } from './Client/register-client/register-client.component';
import { StatsComponent } from './Manager/stats/stats.component';
import { PreferenceComponent } from './Client/preference/preference.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'employe/login', component: LoginEmployeComponent },
  { path: 'client/login', component: LoginClientComponent },
  { path: 'manager/login', component: LoginManagerComponent },
  { path: 'manager/stat', component: StatsComponent },

  { path: 'client/register', component: RegisterClientComponent },
  { path: 'client/preference', component: PreferenceComponent },

  { path: 'employe/accueil', component: AccueilEmployeComponent },
  { path: 'client/accueil', component: AccueilClientComponent },
  { path: 'manager/accueil', component: AccueilManagerComponent },
  { path: 'home', component: HomeComponent },
  {path: '', redirectTo: 'home', pathMatch: 'full'}
  // Autres routes de votre application...
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }