import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartData, ChartEvent, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit{
  alldata:any;
  allCA:any;
  dataLoaded=false;
  isBar=true;
  isLine=false;
  constructor(private managerService: ManagerService){}
  ngOnInit(): void {
    this.managerService.getStatRdv().subscribe((data) => {
      const monthly={
        mensuel:data[0],
        daily:data[1],
      }
      this.alldata = monthly;
      this.barChartDataRDV.datasets[0].data = this.alldata.mensuel.map((item: { date: string, totalRdv: number }) => item.totalRdv);
      this.dataLoaded=true;

    });

    this.managerService.getStatPaiement().subscribe((data) =>
    {
      this.allCA={
        mensuel:data[0],
        daily:data[1],
      }
      this.barChartDataCA.datasets[0].data = this.allCA.mensuel.map((item: { date: string, totalMontant: number }) => item.totalMontant);
      this.dataLoaded=true;
    })

    this.managerService.getBenefice().subscribe((data) =>
    {

      this.barChartDatabenef.datasets[0].data = data[0];
      // this.barChartDatabenef.datasets[1].data = data[1].totalMontant;
      this.barChartDatabenef.datasets[1].data = data[1].map((item: {totalMontant: number }) => item.totalMontant);
      this.barChartDatabenef.datasets[2].data = data[3].map((item: {totalRevenue: number }) => item.totalRevenue);
      this.dataLoaded=true;
    }
    );

  }
 
mois:string[]=  [
  'Janvier',
  'Fevrier',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Août',
  'Septembre',
  'Octobre',
  'Novembre',
  'Decembre',
  
];
  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: [...this.mois],
    datasets: [
      {
        data: [],
        label: 'Rendez-vous mensuel',
        fill: true,
        tension: 0.5,
        borderColor: 'black',
        backgroundColor: 'rgba(255,555,0,255)',
      }
    ]
  };
  public lineChartLegend = true;
  public lineChartOptions: ChartOptions<'line'> = {
    responsive: true,
    maintainAspectRatio: false,  };



  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  public barChartOptions: ChartConfiguration['options'] = {
    elements: {
      line: {tension: 0.4,},
    },
    scales: {
      x: {},
    y: { /*min: 0,*/},
    },
    plugins: {
      legend: { display: true },
    },
    responsive:true,
    maintainAspectRatio:false
  };
  public barChartLabels: string[] = [
    ...this.mois
  ];
  public barChartType: ChartType = 'bar';
  public barChartDataRDV: ChartData<'bar'> = {
    labels: this.barChartLabels,
    datasets: [
      {data:[],label:'Rendez-vous mensuel',        backgroundColor: 'rgba(255,555,0,255)',  },
    ],
  };
  public barChartDataCA: ChartData<'bar'> = {
    labels: this.barChartLabels,
    datasets: [
      {data:[],label:'Chiffre d\'affaire mensuel',        backgroundColor: 'rgba(25,0,255,255)',
    },
    ],
  };
  public barChartDatabenef: ChartData<'bar'> = {
    labels: this.barChartLabels,
    datasets: [
      {data:[],label:'Benefice mensuel',        backgroundColor: 'rgba(0,255,2,255)'},
      {data:[],label:'Dépense mensuelle',        backgroundColor: 'rgba(255,100,20,255)',},
      {data:[],label:'Commission mensuelle',        backgroundColor: 'rgba(100,10,40,2)'},
    ],
  };
 public chartClicked({
    event,
    active,
  }: {
    event?: ChartEvent;
    active?: object[];
  }): void {
    // console.log(event, active);
  }
  public chartHovered({
    event,
    active,
  }: {
    event?: ChartEvent;
    active?: object[];
  }): void {
    // console.log(event, active);
  }
  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
    this.isBar=!this.isBar;
    this.isLine=!this.isLine;
  }
  dating(date:string): string{
    return date.replace('T',' ').split(' ')[0];
  }
  

  }
