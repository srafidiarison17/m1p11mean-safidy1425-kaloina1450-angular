import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';
import { ClientService } from 'src/app/services/client.service';



@Component({
  selector: 'app-accueil-manager',
  templateUrl: './accueil-manager.component.html',
  styleUrls: ['./accueil-manager.component.css']
})
export class AccueilManagerComponent  implements OnInit{
  //  ab=JSON.parse( sessionStorage.getItem('currentUser'));
  user:any;
  nomService:string='';
  prixService:number=0;
  dureeService:number=0;
  commissionService:number=0;
  successMessage: string='';
  descriService: string='';
  showIns:boolean=false;

  showDep:boolean=false;
  successDep:string='';
  titre:string='';
  encaisseur:string='';
  montant:number=0;
  daty:string='';
  type:string='';
  services:any[]=[];

  constructor(private authService: AuthService,private router: Router,private storageService:StorageService,private managerService: ManagerService,private clientService:ClientService) {}

  ngOnInit(): void {
      this.user=this.storageService.getUser();
      this.clientService.getAllService().subscribe(
        (val)=>{this.services=val;
        
        console.log(this.services)},
        (error)=>{console.error(error)},
      );
  }

  insertService() {
      this.authService.saveService(this.nomService,this.prixService,this.dureeService,this.commissionService,this.descriService).subscribe({
        next: data => {
          console.log(data);
          this.nomService='';
          this.prixService=0;
          this.dureeService=0;
          this.descriService='';
          this.commissionService=0;
          this.successMessage='Service inséré';
          this.clientService.getAllService().subscribe((val)=>{this.services=val;});
        },
        error: err => {
        }
      });
  }

  insertDepense(){
    this.managerService.saveDepense(this.titre,this.montant,this.daty,this.type,this.encaisseur).subscribe({
      next: data => {
        console.log(data);
        this.titre='';
        this.montant=0;
        this.daty='';
        this.type='';
        this.encaisseur='';
        this.successDep='Dépense insérée';
      },
      error: err => {
      }
    });
  }
}
