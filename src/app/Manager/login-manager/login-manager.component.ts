import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-login-manager',
  templateUrl: './login-manager.component.html',
  styleUrls: ['./login-manager.component.css']
})
export class LoginManagerComponent {
  username: string = 'kal@gmail.fr';
  password: string = 'fifa';
  errorMessage: string='';

  constructor(private authService: AuthService,private router: Router,private storageService:StorageService) {}

  login() {
    this.errorMessage = ''; // Clear previous error message
    this.authService.login(this.username, this.password,'manager').subscribe(response => {
      if(response!=null){
        this.storageService.saveUser(response);
        // sessionStorage.setItem('currentUser', JSON.stringify(response));  
        this.router.navigate(['/manager/accueil']);    }
    else{
    this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
    }
      console.log(response)
      // Handle successful login response, if needed
    }, error => {
      this.errorMessage = 'Erreur de connexion. Veuillez vérifier vos identifiants.';
      console.error('Login error:', error);
    });
  }
}
