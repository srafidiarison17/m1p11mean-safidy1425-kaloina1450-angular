import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginEmployeComponent } from './Employe/login-employe/login-employe.component';
import { LoginClientComponent } from './Client/login-client/login-client.component';
import { LoginManagerComponent } from './Manager/login-manager/login-manager.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AccueilClientComponent } from './Client/accueil-client/accueil-client.component';
import { AccueilEmployeComponent } from './Employe/accueil-employe/accueil-employe.component';
import { AccueilManagerComponent } from './Manager/accueil-manager/accueil-manager.component';
import { RegisterClientComponent } from './Client/register-client/register-client.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { StatsComponent } from './Manager/stats/stats.component';
import { PreferenceComponent } from './Client/preference/preference.component';
import { NgChartsModule } from 'ng2-charts';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginEmployeComponent,
    LoginClientComponent,
    LoginManagerComponent,
    AccueilClientComponent,
    AccueilEmployeComponent,
    AccueilManagerComponent,
    RegisterClientComponent,
    NavigationBarComponent,
    StatsComponent,
    PreferenceComponent,
    HomeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule ,
    MatMenuModule,
    NgChartsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
